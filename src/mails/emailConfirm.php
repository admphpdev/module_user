<?php
use yii\helpers\Html;
use amd_php_dev\module_user\Module as UserModule;
 
/* @var $this yii\web\View */
/* @var $user amd_php_dev\module_user\models\User */
 
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$emailConfirmUrlPath, 
    'token' => $user->email_confirm_token
]);
?>
 
Здравствуйте, <?= Html::encode($user->username) ?>!
 
Для подтверждения адреса пройдите по ссылке:
 
<?= Html::a(Html::encode($confirmLink), $confirmLink) ?>
 
Если Вы не регистрировались у на нашем сайте, то просто удалите это письмо.