<?php
use yii\helpers\Html;
use amd_php_dev\module_user\Module as UserModule;
 
/* @var $this yii\web\View */
/* @var $user amd_php_dev\module_user\models\User */
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 'token' => $user->password_reset_token
]);
?>
 
Здравствуйте, <?= Html::encode($user->username) ?>!
 
Пройдите по ссылке, чтобы сменить пароль:
 
<?= Html::a(Html::encode($resetLink), $resetLink) ?>