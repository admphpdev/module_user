<?php
use amd_php_dev\module_user\Module as UserModule;

/* @var $this yii\web\View */
/* @var $user amd_php_dev\module_user\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 
    'token' => $user->password_reset_token
]);
?>
Hello <?= $user->username ?>,

Follow the link below to reset your password:

<?= $resetLink ?>