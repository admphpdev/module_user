<?php
use yii\helpers\Html;
use amd_php_dev\module_user\Module as UserModule;

/* @var $this yii\web\View */
/* @var $user amd_php_dev\module_user\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    UserModule::$passwordResetUrlPath, 
    'token' => $user->password_reset_token
]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->username) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>