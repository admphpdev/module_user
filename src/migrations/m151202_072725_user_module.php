<?php

use \amd_php_dev\module_user\models\User as UserModel;
use \amd_php_dev\module_user\models\UserProfile as UserProfile;
use \amd_php_dev\yii2_components\migrations\generators;

class m151202_072725_user_module extends \amd_php_dev\yii2_components\migrations\Migration
{
    public $userTableName               = '{{%user}}';
    public $profileTableName            = '{{%user_profile}}';
    public $roleTableName               = '{{%user_role}}';
    public $optionTableName             = '{{%user_option}}';
    public $optionGroupTableName        = '{{%user_option_group}}';
    public $optionToRoleTableName       = '{{%user_option_to_role}}';
    public $optionValueTableName        = '{{%user_option_value}}';
    public $optionGroupToRoleTableName  = '{{%user_option_group_to_role}}';

    public function init()
    {
        parent::init();

        $this->_rbacMigrate();
    }

    public function up()
    {
        // Создаём таблицу пользователей
        $userGenerator = new generators\User($this, $this->userTableName);
        $userGenerator->create();

        // Создаём таблицу профилей пользователей
        $profileGenerator = new generators\Profile($this, $this->profileTableName);
        $profileGenerator->additionalColumns['phone_index'] = $this->integer();
        $profileGenerator->addIndex('phone_index');
        $profileGenerator->additionalColumns['about_text'] = $this->text();
        $profileGenerator->create();

        $generator = new \amd_php_dev\yii2_components\migrations\generators\UserRole($this, $this->roleTableName);
        $generator->create();

        $generator = new \amd_php_dev\yii2_components\migrations\generators\Option($this, $this->optionTableName);
        $generator->additionalColumns['all'] = $this->boolean()->defaultValue(false);
        $generator->addIndex('all');
        $generator->additionalColumns['id_group'] = $this->integer()->defaultValue(1);
        $generator->addIndex('id_group');
        $generator->create();

        $this->createTable($this->optionToRoleTableName, [
            'id_item' => \yii\db\Schema::TYPE_INTEGER . ' NOT NULL',
            'id_related' => \yii\db\Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->addPrimaryKey('', $this->optionToRoleTableName, ['id_item', 'id_related']);

        $generator = new \amd_php_dev\yii2_components\migrations\generators\OptionGroup($this, $this->optionGroupTableName);
        $generator->additionalColumns['all'] = $this->boolean()->defaultValue(false);
        $generator->additionalColumns['free_field'] = $this->boolean()->defaultValue(false);
        $generator->additionalColumns['id_parent'] = $this->integer()->defaultValue(0);
        $generator->addIndex('all');
        $generator->create();

        $this->createTable($this->optionGroupToRoleTableName, [
            'id_item' => \yii\db\Schema::TYPE_INTEGER . ' NOT NULL',
            'id_related' => \yii\db\Schema::TYPE_STRING . ' NOT NULL',
        ]);
        $this->addPrimaryKey('', $this->optionGroupToRoleTableName, ['id_item', 'id_related']);

        $generator = new \amd_php_dev\yii2_components\migrations\generators\OptionValue($this, $this->optionValueTableName);
        $generator->create();

        $this->insert(
            $this->optionGroupTableName,
            [
                'active'    => \amd_php_dev\yii2_components\models\SmartRecord::ACTIVE_ACTIVE,
                'name'      => 'Общие характеристики',
                'code'      => 'MAIN',
                'all'       => true,
            ]
        );

        // Добавляем пользователя админ
        $user   = new UserModel();
        $user->username = 'admin';
        $user->email    = 'admin@admin.admin';
        $user->setPassword('admin');
        $user->active   = UserModel::ACTIVE_ACTIVE;
        $user->role     = UserModel::ROLE_ADMIN;
        $user->generateAuthKey();
        $user->save();

        // Добавляем профиль пользователю админ
        $profile = new UserProfile();
        $profile->id_user = $user->id;
        $profile->fullname = 'Админов Админ АдминЫЧ';
        $profile->phone_mobile = '8 800 555 35 35';
        $profile->save();

        $this->_rbacInit();
        $rbacData = $this->_createRbacData();
        $users = UserModel::find()->asArray()->all();
        $this->_assignUsers($rbacData, $users);
    }

    public function down()
    {
        $tables = get_class_vars($this);

        foreach ($tables as $table) {
            if (preg_match('/^\S+TableName$/', $table)) {
                $this->dropTable($table);
            }
        }
    }

    /**
     * Создаёт массив ролей
     *
     * @return array
     */
    protected function _createRbacData()
    {
        $result = [];
        $roles = UserModel::getRolesArray();

        // Роль user
        $model = new \amd_php_dev\module_user\models\UserRole();
        $model->name = 'Пользователь';
        $model->role = UserModel::ROLE_USER;
        $model->active = UserModel::ACTIVE_ACTIVE;
        $model->save();

        $role = \Yii::$app->authManager->createRole(UserModel::ROLE_USER);
        $role->description = $roles[UserModel::ROLE_USER];
        \Yii::$app->authManager->add($role);
        $result[UserModel::ROLE_USER] = $role;

        // Роль журналист
        $model = new \amd_php_dev\module_user\models\UserRole();
        $model->name = 'Журналист';
        $model->role = UserModel::ROLE_JOURNALIST;
        $model->parent = UserModel::ROLE_USER;
        $model->active = UserModel::ACTIVE_ACTIVE;
        $model->save();

        $role = \Yii::$app->authManager->createRole(UserModel::ROLE_JOURNALIST);
        $role->description = $roles[UserModel::ROLE_JOURNALIST];
        \Yii::$app->authManager->add($role);
        \Yii::$app->authManager->addChild($role, $result[UserModel::ROLE_USER]);
        $result[UserModel::ROLE_JOURNALIST] = $role;

        // Роль Модератор
        $model = new \amd_php_dev\module_user\models\UserRole();
        $model->name = 'Модератор';
        $model->role = UserModel::ROLE_MODERATOR;
        $model->parent = UserModel::ROLE_JOURNALIST;
        $model->active = UserModel::ACTIVE_ACTIVE;
        $model->save();

        $role = \Yii::$app->authManager->createRole(UserModel::ROLE_MODERATOR);
        $role->description = $roles[UserModel::ROLE_MODERATOR];
        \Yii::$app->authManager->add($role);
        \Yii::$app->authManager->addChild($role, $result[UserModel::ROLE_JOURNALIST]);
        $result[UserModel::ROLE_MODERATOR] = $role;

        // Роль Администратор
        $model = new \amd_php_dev\module_user\models\UserRole();
        $model->name = 'Администратор';
        $model->role = UserModel::ROLE_ADMIN;
        $model->parent = UserModel::ROLE_MODERATOR;
        $model->active = UserModel::ACTIVE_ACTIVE;
        $model->save();

        $role = \Yii::$app->authManager->createRole(UserModel::ROLE_ADMIN);
        $role->description = $roles[UserModel::ROLE_ADMIN];
        \Yii::$app->authManager->add($role);
        \Yii::$app->authManager->addChild($role, $result[UserModel::ROLE_MODERATOR]);
        $result[UserModel::ROLE_ADMIN] = $role;

        return $result;
    }

    /**
     * Создаём миграцию
     */
    protected function _rbacMigrate() {
        \Yii::$app->controller->run('migrate/up', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
    }

    protected function _rbacInit()
    {
        \Yii::$app->authManager->removeAll();
    }

    /**
     * Привязывает роли к пользователям
     *
     * @param array $roles Существующие роли
     * @param array $users пользователи
     */
    protected function _assignUsers($roles, $users)
    {
        foreach ($users as $user) {
            \Yii::$app->authManager->assign($roles[$user['role']], $user['id']);
        }
    }
}
