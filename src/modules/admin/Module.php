<?php

namespace amd_php_dev\module_user\modules\admin;

class Module extends \amd_php_dev\yii2_components\modules\Admin
{
    public $controllerNamespace = 'amd_php_dev\module_user\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Пользователи',
                    'items' => [
                        ['label' => 'Пользователи', 'url' => ['/user/admin/user/index']],
                        ['label' => 'Роли', 'url' => ['/user/admin/role/index']],
                        ['label' => 'Характеристики', 'url' => ['/user/admin/option/index']],
                        ['label' => 'Группы характеристик', 'url' => ['/user/admin/option-group/index']],
                    ]
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
