<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_user\models\UserOption */
/* @var $form yii\widgets\ActiveForm */

$allFormName = $model->formName() . '[all]';
$rolesFormName = $model->formName() . '[' .
    \amd_php_dev\module_user\models\UserOption::ATTR_ROLES .
    '][]';
?>

<div class="user-option-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'active',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'priority',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'name',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'all',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$allFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$rolesFormName}"]').val(null);
            $('[name="{$rolesFormName}"]').trigger('change');
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => \amd_php_dev\module_user\models\UserOption::ATTR_ROLES,
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$rolesFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$allFormName}"]').val(0);
        } else {
            $('[name="{$allFormName}"]').val(1);
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'id_group',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?php
    $modelFormName = $model->formName() . '[id_group]';
    $ajaxUrl = \yii\helpers\Url::to(['option-group/get-by-params']);

    $script = <<<CODE

$(document).ready(function() {
    $(document).on('change', '[name="{$allFormName}"], [name="{$rolesFormName}"]', function() {
        var data = {
            'roles': $('[name="{$rolesFormName}"]').val(),
        };

        $.ajax({
            'url': '{$ajaxUrl}',
            'method': 'POST',
            'data': data,
            'dataType': 'json',
            'success': function(res) {
                var input = $('[name="{$modelFormName}"]');
                input.html('').select2({data: null});
                if (res.length) {
                    input.select2(
                        {
                            'data': res
                        }
                    );
                }
            }
        });
    });
});
CODE;

    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'in_filter',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'required',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'image',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'type',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'variants',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'default',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
            'model'     => $model,
            'attribute' => 'description',
            'label'     => true,
            'form'      => $form,
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
