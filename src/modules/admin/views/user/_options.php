<?php if (!empty($form)) : ?>
    <div class="options-set" id="item-options">

    <?php
    $roleFormName = $model->formName() . '[role]';
    $ajaxUrl = \yii\helpers\Url::to(['options-by-params']);

    $script = <<<CODE

$(document).ready(function() {
    $(document).on('change', '[name="{$roleFormName}"]', function() {
        var role = $('[name="{$roleFormName}"]').val();
        var itemId = window.lib.helpers.getQueryParamByName('id');

        var data = {
            'role': role,
            'itemId': itemId,
        };
        $.ajax({
            'url': '{$ajaxUrl}',
            'method': 'POST',
            'data': data,
            'dataType': 'html',
            'success': function(res) {
                $('#item-options').html(res);
            }
        });
    });
});
CODE;

    $this->registerJs($script, \yii\web\View::POS_END);
    ?>
<?php else :
    $form = new \amd_php_dev\yii2_components\widgets\form\DummyActiveForm();
    $form->options['id'] = 'item-form';
    ?>
<?php endif; ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_user\models\User::ATTR_OPTIONS,
        'label'     => true,
        'form'      => $form,
    ]); ?>

<?php if (!($form instanceof  \amd_php_dev\yii2_components\widgets\form\DummyActiveForm)) : ?>
    </div>
<?php else :
    $form->run();
    ?>
<?php endif; ?>

        