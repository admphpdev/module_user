<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'active',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'username',
        'label'     => true,
        'form'      => $form,
    ]); ?>


    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'email',
        'label'     => true,
        'form'      => $form,
    ]); ?>


    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'password_hash',
        'label'     => true,
        'form'      => $form,
    ]); ?>


    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'role',
        'label'     => true,
        'form'      => $form,
    ]); ?>

</div>
