<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_user\models\User */
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile,
    ]) ?>

</div>
