<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_user\models\User */
/* @var $profile amd_php_dev\module_user\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin([
    'options' => ['enctype'=>'multipart/form-data']
]);

echo Tabs::widget([
    'items' => [
        [
            'label' => 'Пользователь',
            'content' => $this->render('_userForm', ['model' => $model, 'form' => $form]),
            'active' => true
        ],
        [
            'label' => 'Профиль',
            'content' => $this->render('_profileForm', ['model' => $profile, 'form' => $form]),
        ],
        [
            'label' => 'Характеристики',
            'content' => $this->render('_options', ['model' => $model, 'form' => $form]),
        ],
    ],
]);
?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>


