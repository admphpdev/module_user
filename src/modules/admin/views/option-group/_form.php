<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model amd_php_dev\module_user\models\UserOptionGroup */
/* @var $form yii\widgets\ActiveForm */

$allFormName = $model->formName() . '[all]';
$rolesFormName = $model->formName() . '[' .
    \amd_php_dev\module_user\models\UserOptionGroup::ATTR_ROLES .
    '][]';
?>

<div class="user-option-group-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'active',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'priority',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'name',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'all',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$allFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$rolesFormName}"]').val(null);
            $('[name="{$rolesFormName}"]').trigger('change');
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => \amd_php_dev\module_user\models\UserOption::ATTR_ROLES,
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <?php
    $script = <<<SCRIPT
$(document).ready(function() {
    $(document).on('change', '[name="{$rolesFormName}"]', function() {
        if ($(this).val()) {
            $('[name="{$allFormName}"]').val(0);
        } else {
            $('[name="{$allFormName}"]').val(1);
        }
    });
});
SCRIPT;
    $this->registerJs($script, \yii\web\View::POS_END);
    ?>

    <?= \amd_php_dev\yii2_components\widgets\form\SmartInput::widget([
        'model'     => $model,
        'attribute' => 'image',
        'label'     => true,
        'form'      => $form,
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
