<?php

namespace amd_php_dev\module_user\modules\admin\controllers;

use Yii;
use amd_php_dev\module_user\models\User;
use amd_php_dev\module_user\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends \amd_php_dev\yii2_components\controllers\PublicController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionOptionsByParams()
    {
        $this->layout = false;

        $modelId = (int) \yii::$app->request->post('itemId');
        $roleItem = \amd_php_dev\module_user\models\UserRole::find()
            ->andWhere('role = :role', ['role' => \yii::$app->request->post('role')])->one();
        $role = $roleItem->id;

        if (!empty($modelId)) {
            $model = $this->findModel($modelId);
        } else {
            $model = new \amd_php_dev\module_user\models\User();
        }

        $optionsManager = $model->getBehavior('optionsManager');

        $optionsManager->options = $model->getOptionsRelation([$role])->all();
        $optionsManager->optionGroups = $model->getOptionGroupsRelation([$role])->all();

        echo $this->renderAjax('_options', ['model' => $model]);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = 'Пользователи';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $profile = new \amd_php_dev\module_user\models\UserProfile();

        if ($this->saveUser($model) && $this->saveProfile($profile, $model)) {
            $model->assignRole();
            return $this->redirect(['index']);
        } else {

            $this->view->title = 'Добавить пользователя';
            $this->view->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('create', [
                'model' => $model,
                'profile' => $profile,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!$profile = $model->getProfileRelation()->one()) {
            $profile = new \amd_php_dev\module_user\models\UserProfile();
        }

        if ($this->saveUser($model) && $this->saveProfile($profile, $model)) {
            if ($model->oldAttributes['role'] != $model->role) {
                $model->assignRole();
            }
            return $this->redirect(['index']);
        } else {
            $this->view->title = 'Редактировать пользователя: ' . ' ' . $model->email;
            $this->view->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
            $this->view->params['breadcrumbs'][] = $this->view->title;
            
            return $this->render('update', [
                'model' => $model,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $profile \amd_php_dev\module_user\models\UserProfile
     * @param $user \amd_php_dev\module_user\models\User
     * @return mixed
     */
    protected function saveProfile($profile, $user)
    {
        $profile->load(Yii::$app->request->post());

        $profile->id_user = $user->id;

        return $profile->save();
    }

    /**
     * @param $user \amd_php_dev\yii2_components\models\User
     */
    protected function saveUser($user)
    {
        if (!$user->load(Yii::$app->request->post())) {
            return false;
        }

        $password = $_POST[$user->formName()]['password_hash'];

        if ($user->isNewRecord || ($user->oldAttributes['password_hash'] != $password)) {
            $user->setPassword($password);
        }

        return $user->save();
    }
}
