<?php

namespace amd_php_dev\module_user\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use amd_php_dev\module_user\models\LoginForm;
use amd_php_dev\module_user\models\EmailConfirmForm;
use amd_php_dev\module_user\models\PasswordResetRequestForm;
use amd_php_dev\module_user\models\PasswordResetForm;
use amd_php_dev\module_user\models\SignupForm;
use amd_php_dev\module_user\components\helpers;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

/**
 * Контроллер для основных действие модуля User
 */
class DefaultController extends \amd_php_dev\yii2_components\controllers\PublicController
{
    public function init()
    {
        parent::init();
        $this->view->params['jsScenarios'] = [
            '/builds/index.js'
        ];

        $this->view->params['cssScenarios'] = [
            '/builds/index.css'
        ];
    }

    /**
     * Список поведений контроллера
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions'   => ['signup'],
                        'allow'     => true,
                        'roles'     => ['?']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * Список действий-классов контроллера
     * @return array
     */
    public function actions() {
         return [
            'captcha-signup' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'captcha-login' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Действие входа пользователя в систему
     * @return string HTML вывод
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
 
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {

            $this->view->title = 'Авторизация';
            $this->view->params['breadcrumbs'][] = $this->view->title;

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Действие выхода пользователя из системы
     * @return string HTML вывод
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
 
        return $this->goHome();
    }
    
    /**
     * Действие запроса на регистрацию пользователя
     * @return string HTML вывод
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {

                $emailHost = helpers\UserHelper::getEmailHost($user->email);
                if ($emailHost) {
                    $flashMessage = "Подтвердите Ваш email " .
                        "<a href=\"//{$emailHost}\" target='_blank'>{$emailHost}</a>. Email администратора: " .
                        \Yii::$app->params['SUPPORT_EMAIL'];
                } else {
                    $flashMessage = 'Подтвердите регистрацию на Вашей почте. Email администратора: ' .
                        \Yii::$app->params['SUPPORT_EMAIL'];
                }
                
                Yii::$app->getSession()->setFlash('success', $flashMessage);
                
                return $this->goHome();
            }
        }

        $this->view->title = 'Регистрация';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
     * Подтверждение регистрации пользователя по выданному ему коду
     * В случае не актуальности кода - http ошибка
     * @param string $token Код назначеный пользователю при запросе на регистрацию
     * @return void
     * @throws BadRequestHttpException 
     */
    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
 
        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('success', 'Спасибо! Ваш Email успешно подтверждён.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка подтверждения Email. Свяжитесь с администратором: ' .
                \Yii::$app->params['SUPPORT_EMAIL']);
        }
 
        return $this->goHome();
    }
    
    /**
     * Действие запроса на изменения пароля пользователя
     * @return string HTML вывод
     */
    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                $emailHost = helpers\UserHelper::getEmailHost($model->email);
                if ($emailHost) {
                    $flashMessage = "Спасибо! На ваш <a href=\"//{$emailHost}\" target='_blank'>Email</a> было отправлено письмо со ссылкой на восстановление пароля.";
                } else {
                    $flashMessage = 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.';
                }
                $flashMessage .= "Email администратора: " . \Yii::$app->params['SUPPORT_EMAIL'];
                Yii::$app->getSession()->setFlash('success', $flashMessage);
 
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
            }
        }

        $this->view->title = 'Запрос сброса пароля';
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('passwordResetRequest', [
            'model' => $model,
        ]);
    }
    
    /**
     * Подтверждение запроса и изменение пароля пользователя
     * @param string $token Код назначеный пользователю при запросе на изменение пароля
     * @return string HTML вывод
     * @throws BadRequestHttpException
     */
    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
 
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Спасибо! Пароль успешно изменён.');
 
            return $this->goHome();
        }

        $this->view->title = 'Сброс пароля';
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }
}
