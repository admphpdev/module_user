<?php
/**
 * Created by PhpStorm.
 * User: v-11
 * Date: 30.11.2016
 * Time: 18:38
 */

namespace amd_php_dev\module_user\models;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends \amd_php_dev\yii2_components\models\OptionQuery
{

    /**
     * @inheritdoc
     * @return UserOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byOption($code, $value)
    {
        return $this->leftJoin('{{%user_option_value}}', '{{%user_option_value}}.id_item = {{%user}}.id')
            ->leftJoin('{{%user_option}}', '{{%user_option}}.id = {{%user_option_value}}.id_option')
            ->defaultScope()
            ->andWhere('{{%user_option}}.code = :code', ['code' => $code])
            ->andWhere('{{%user_option_value}}.value = :value', ['value' => $value]);
    }
}