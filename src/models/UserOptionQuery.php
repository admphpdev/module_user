<?php

namespace amd_php_dev\module_user\models;

/**
 * This is the ActiveQuery class for [[UserOption]].
 *
 * @see UserOption
 */
class UserOptionQuery extends \amd_php_dev\yii2_components\models\OptionQuery
{

    use OptionAndGroupQueryTrait;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'rolesManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\taggable\TaggableQueryBehavior::className(),
                'tagRelation' => 'rolesRelation',
                'tagValueAttribute' => 'id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     * @return UserOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
