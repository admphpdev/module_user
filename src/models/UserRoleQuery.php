<?php

namespace amd_php_dev\module_user\models;

/**
 * This is the ActiveQuery class for [[UserRole]].
 *
 * @see UserRole
 */
class UserRoleQuery extends \amd_php_dev\yii2_components\models\SmartQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return UserRole[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserRole|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
