<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 20.11.15
 * Time: 15:01
 */

namespace amd_php_dev\module_user\models;

use amd_php_dev\yii2_components\models\SmartRecord;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $username
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $role
 * @property integer $active
 */
class User extends \amd_php_dev\yii2_components\models\User
{
    const ATTR_OPTIONS = 'optionValues';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'optionsManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\OptionBehavior::className(),
                'setableAttribute' => static::ATTR_OPTIONS,
                'optionsRelation' => 'optionsRelation',
                'optionValuesRelation' => 'optionValuesRelation',
                'optionGroupsRelation' => 'optionGroupsRelation',
            ],
        ]);
    }

    public function getItemUrl()
    {
        $url = '';
        $managerUrl = '';

        if (!$this->isNewRecord) {
            $optionsManager = $this->getBehavior('optionsManager');
            $managerUrl = $optionsManager->getOptionsBy('code', 'url')->getOptionValue();
        }

        if (empty($managerUrl)) {
            $url = '#';
        } else {
            $url = 'http://' . $managerUrl . '.' . \yii::$app->params['HOST'];
        }

        return $url;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            [static::ATTR_OPTIONS, 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            static::ATTR_OPTIONS => 'Характеристики'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case self::ATTR_OPTIONS:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_OPTIONS;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case self::ATTR_OPTIONS:
                $result = $this->getBehavior('optionsManager');
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    public function getRoleRelation()
    {
        return $this->hasOne(\amd_php_dev\module_user\models\UserRole::className(), ['role' => 'role']);
    }

    public function getProfileRelation()
    {
        return $this->hasOne(\amd_php_dev\module_user\models\UserProfile::className(), ['id_user' => 'id']);
    }

    /**
     * @return \amd_php_dev\yii2_components\models\PageQuery
     */
    public function getOptionsRelation($role = null)
    {
        if (!empty($role)) {
            $idRole = $role;
        } elseif (!empty($this->role)) {
            $role = $this->getRoleRelation()->asArray()->one();
            $idRole = $role['id'];
        }

        if (!isset($idRole)) {
            return \amd_php_dev\module_user\models\UserOption::find()
                ->onlyWithAll()
                ->indexBy('id');
        } else {
            return \amd_php_dev\module_user\models\UserOption::find()
                ->byRoles($idRole)
                ->indexBy('id');
        }
    }

    /**
     * @return \amd_php_dev\yii2_components\models\PageQuery
     */
    public function getOptionGroupsRelation($role = null)
    {
        if (!empty($role)) {
            $idRole = $role;
        } elseif (!empty($this->role)) {
            $role = $this->getRoleRelation()->asArray()->one();
            $idRole = $role['id'];
        }

        if (!isset($idRole)) {
            return \amd_php_dev\module_user\models\UserOptionGroup::find()
                ->onlyWithAll()
                ->indexBy('id');
        } else {
            return \amd_php_dev\module_user\models\UserOptionGroup::find()
                ->byRoles($idRole)
                ->indexBy('id');
        }
    }

    /**
     * @return \yii\db\Query
     */
    public function getOptionValuesRelation()
    {
        $query = new \yii\db\Query();
        return $query->select('*')
            ->from('{{%user_option_value}}')
            ->where('id_item = ' . (int) $this->id)
            ->indexBy('id_option');
    }
}