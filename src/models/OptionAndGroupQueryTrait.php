<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 27.04.16
 * Time: 16:34
 */

namespace amd_php_dev\module_user\models;


trait OptionAndGroupQueryTrait
{
    public function byRoles($role = null, $withAll = true)
    {
        $toUnoin = false;
        if ($withAll) {
            $this->defaultScope()->onlyWithAll();
            $toUnoin = true;
        }
        /**
         * @var $model CatalogOptionGroup
         */
        $model = new $this->owner->modelClass();

        if (!empty($role)) {
            if (is_array($role)) {
                $roles = $role;
            } else {
                $roles = [$role];
            }
            $roles = \amd_php_dev\yii2_components\helpers\DbHelper::quoteArray($roles);

            /**
             * Добавляем в запрос роли
             */
            $rolesRelation = $model->getRolesRelation();
            $optGrpToRoleTableName = $rolesRelation->via->from[0];
            $roleTableName = call_user_func(
                $rolesRelation->modelClass .
                '::tableName'
            );

            /**
             * @var $query UserOptionGroupQuery
             */
            if (empty($withAll)) {
                $query = $this;
            } else {
                $query = $model->find();
            }
            $query->defaultScope()
                ->innerJoin(
                    $optGrpToRoleTableName,
                    $this->getModelTableName() . '.`id` = ' . $optGrpToRoleTableName . '.`id_item`'
                )
                ->innerJoin(
                    $roleTableName,
                    "$optGrpToRoleTableName.`id_related` = $roleTableName.`id`"
                )
                ->andWhere("$roleTableName.`id` IN (" . $roles . ')')
                ->groupBy($this->getModelTableName() . '.`id`');

            if ($toUnoin) {
                $this->union($query);
            } else {
                $toUnoin = true;
            }
        }

        return $this;
    }

    public function onlyWithAll()
    {
        $this->andWhere($this->getModelTableName() . '.`all` = ' . \amd_php_dev\module_user\models\UserOption::ALL_TRUE);
        return $this;
    }
}