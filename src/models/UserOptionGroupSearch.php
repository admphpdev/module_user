<?php

namespace amd_php_dev\module_user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use amd_php_dev\module_user\models\UserOptionGroup;

/**
 * UserOptionGroupSearch represents the model behind the search form about `amd_php_dev\module_user\models\UserOptionGroup`.
 */
class UserOptionGroupSearch extends UserOptionGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'image', 'name'], 'safe'],
            [['active', 'priority', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserOptionGroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'active' => $this->active,
            'priority' => $this->priority,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
