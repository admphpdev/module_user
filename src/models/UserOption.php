<?php

namespace amd_php_dev\module_user\models;

use Yii;

/**
 * This is the model class for table "{{%user_option}}".
 *
 * @property integer $active
 * @property integer $priority
 * @property integer $id
 * @property integer $id_group
 * @property integer $in_filter
 * @property integer $required
 * @property string $code
 * @property string $image
 * @property string $all
 * @property string $type
 * @property string $name
 * @property string $variants
 * @property string $default
 * @property string $description
 */
class UserOption extends \amd_php_dev\yii2_components\models\Option
{
    const IMAGES_URL_ALIAS = '@web/data/images/user/option/';
    
    const ALL_TRUE = 1;
    const ALL_FALSE = 0;

    const ATTR_ROLES = 'roles';

    public static function getAllArray()
    {
        return [
            static::ALL_TRUE    => 'Да',
            static::ALL_FALSE   => 'Нет',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_option}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'rolesManager' => [
                'class' => \amd_php_dev\yii2_components\behaviors\taggable\TaggableBehavior::className(),
                'tagRelation' => 'rolesRelation',
                'tagValueAttribute' => 'id',
                'tagValueType' => 'id',
                'tagFrequencyAttribute' => false,
                'tagValuesAttribute' => static::ATTR_ROLES,
            ],
        ]);
    }

    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'id_group':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_SELECT;
                break;
            case 'all':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case self::ATTR_ROLES:
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_CATEGORIES;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            case self::ATTR_ROLES:
                $data = $this->getRolesRelation()->clean()->defaultScope()->asArray()->all();
                $result = [];
                foreach ($data as $item) {
                    $result[$item['id']] = $item['role'] . ' - ' . $item['name'];
                }
                break;
            case 'all':
                $result = static::getAllArray();
                break;
            case 'id_group':
                $result = [];
                if (!$this->isNewRecord) {
                    $roles = array_map(
                        function($e) {
                            return $e['id'];
                        },
                        $this->getRolesRelation()
                            ->defaultScope()->asArray()->all()
                    );
                    $query = $this->getGroupRelation()->clean()->byRoles($roles);
                } elseif (method_exists($this, 'search')) {
                    $query = $this->getGroupRelation()->clean();
                } else {
                    $query = $this->getGroupRelation()->clean()->byRoles(null);
                }

                $data = $query->asArray()->all();

                foreach ($data as $item) {
                    $result[$item['id']] = $item['id'] . ' - ' . $item['name'];
                }
                break;
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['id_group'], 'integer'],
            [['id_group'], 'default', 'value' => 1],
            ['all', 'default', 'value' => static::ALL_FALSE],
            ['all', 'integer'],
            [static::ATTR_ROLES, 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'id_group' => 'Группа',
            'all' => 'Для всех',
            static::ATTR_ROLES => 'Роли',
        ]);
    }

    /**
     * @inheritdoc
     * @return UserOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserOptionQuery(get_called_class());
    }

    public function getRolesRelation()
    {
        return $this->hasMany(
            \amd_php_dev\module_user\models\UserRole::className(), ['id' => 'id_related'])
            ->viaTable('{{%user_option_to_role}}', ['id_item' => 'id']);
    }

    public function getGroupRelation()
    {
        return $this->hasOne(
            \amd_php_dev\module_user\models\UserOptionGroup::className(),
            ['id' => 'id_group']
        );
    }
}
