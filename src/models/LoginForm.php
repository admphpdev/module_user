<?php

namespace amd_php_dev\module_user\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;
    public $verifyCode;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // email must be a valid email
            ['email', 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // Правила для поля verifyCode
            ['verifyCode', 'captcha', 'captchaAction' => '/user/default/captcha-login'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Неверный email пользователя или пароль.');
            } elseif ($user && $user->active == User::ACTIVE_BLOCKED) {
                $this->addError('email', 'Ваш аккаунт заблокирован, свяжитесь с администратором: ' . \Yii::$app->params['SUPPORT_EMAIL']);
            } elseif ($user && $user->active == User::ACTIVE_WAIT) {
                $this->addError('email', 'Ваш аккаунт не активирован, свяжитесь с администратором: ' . \Yii::$app->params['SUPPORT_EMAIL']);
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'         => 'Email',
            'password'      => 'Пароль',
            'rememberMe'    => 'Запомнить меня',
            'verifyCode'    => 'Код',
        ];
    }
}
