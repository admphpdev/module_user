<?php
namespace amd_php_dev\module_user\models;
 
use yii\base\Model;
use Yii;
 
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verifyCode;
 
    public function rules()
    {
        return [
            // Правила для поля username
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 3, 'max' => 255],
            
            // Правила для поля email
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'Пользователь с таким email уже существует'],
            
            // Правила для поля password
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            // Правила для поля verifyCode
            ['verifyCode', 'captcha', 'captchaAction' => '/user/default/captcha-signup'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'username'      => 'Имя',
            'email'         => 'Email',
            'password'      => 'Пароль',
            'verifyCode'    => 'Проверочный код',
        ];
    }
 
    /**
     * Signs user up.
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->active = User::ACTIVE_WAIT;
            $user->role = User::ROLE_USER;
            $user->generateAuthKey();
            $user = $this->generateEmailConfirmToken($user);

            return $user;
        }
 
        return null;
    }
    
    
    /**
     * Generate email confirm token.
     * 
     * @param User $user User model instance
     * @param boolean $save To save User model
     * @return User|null the saved model or null if saving fails
     */
    public function generateEmailConfirmToken($user, $save = true) 
    {
        $user->generateEmailConfirmToken();
        if ($save && $user->save()) {
            $res = $this->sendConfirmMail($user);
        }
        
        if (!$save && $user->validate()) {
            $res = $this->sendConfirmMail($user);
        }
        
        return !empty($res) ? $user : null;
    }
    
    /**
     * Отправляет сообщение с кодом подтверждения
     * 
     * @param User $user User model instance
     * @return boolean Send mail status
     */
    public function sendConfirmMail($user) 
    {
        return Yii::$app->mailer->compose('@app/modules/user/mails/emailConfirm', ['user' => $user])
            ->setFrom([Yii::$app->params['SUPPORT_EMAIL'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Подтверждение регистрации для ' . Yii::$app->name)
            ->send();
    }
}