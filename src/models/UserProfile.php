<?php

namespace amd_php_dev\module_user\models;

use Yii;
use \amd_php_dev\yii2_components\behaviors\ImageUploadBehavior;

/**
 * This is the model class for table "{{%user_profile}}".
 *
 * @property integer $active
 * @property integer $priority
 * @property integer $id
 * @property integer $id_user
 * @property string $fullname
 * @property string $phone_mobile
 * @property string $image_full
 * @property string $image_small
 */
class UserProfile extends \amd_php_dev\yii2_components\models\SmartRecord
{
    const IMAGES_URL_ALIAS = '@web/data/images/user/profile/';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image_small',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_THUMB.MAX_WIDTH'] : 300,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_THUMB.MAX_HEIGHT'] : 400,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
            [
                'class' => ImageUploadBehavior::className(),
                'attribute' => 'image_full',
                'createThumbsOnSave' => false,
                'maxWidth' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_WIDTH']) ? \Yii::$app->params['IMAGE_FULL.MAX_WIDTH'] : 600,
                'maxHeight' => !empty(\Yii::$app->params['IMAGE_FULL.MAX_HEIGHT']) ? \Yii::$app->params['IMAGE_FULL.MAX_HEIGHT'] : 800,
                'fileUrl' => \Yii::getAlias(static::IMAGES_URL_ALIAS, false),
            ],
        ]);
    }

    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'image_small':
            case 'image_full':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_IMAGE_SINGLE;
                break;
            case 'id_user':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REGULAR_SELECT;
                break;
            case 'phone_index':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_READONLY;
                break;
            case 'about_text':
                $result = \amd_php_dev\yii2_components\widgets\form\SmartInput::TYPE_REDACTOR;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            ['active', 'default', 'value' => static::ACTIVE_ACTIVE],
            ['id_user', 'integer'],
            ['id_user', 'unique'],
            ['about_text', 'string'],
            [['fullname', 'phone_mobile'], 'string', 'max' => 255],
            [['image_small', 'image_full'], 'safe'],
            ['image_small', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
            ['image_full', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
        ]);
        /*return [

        ];*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
            'fullname' => 'Полное имя',
            'phone_mobile' => 'Моб. телефон',
            'image_small' => 'Аватар мал.',
            'image_full' => 'Аватар',
            'phone_index' => 'Индекс телефона',
            'about_text' => 'О себе',
        ]);
    }

    /**
     * @inheritdoc
     * @return UserProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserProfileQuery(get_called_class());
    }
}
