<?php

namespace amd_php_dev\module_user;

class Module extends \amd_php_dev\yii2_components\modules\Module
{
    use \amd_php_dev\yii2_components\modules\ComposerModuleTrait;
    protected $_urlRules = [
        [
            'rules' => [
                '<action:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'user/default/<action>',
            ],
            'append' => false,
        ],
    ];
    public $controllerNamespace = 'amd_php_dev\module_user\controllers';

    // Пути к важным действиям контроллеров
    public static $emailConfirmUrlPath = '/user/default/email-confirm';
    public static $passwordResetUrlPath = '/user/default/password-reset';
    public static $passwordResetRequestUrlPath = '/user/default/password-reset-request';

    public function init()
    {
        parent::init();

        $this->modules = [
            'admin' => [
                'class' => 'amd_php_dev\module_user\modules\admin\Module',
            ]
        ];
    }
}
